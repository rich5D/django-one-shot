## ToDo List

- [ ] Create a Django Project
- [ ] Create a django app
- [ ] write two models
- [ ] write seven views
- [ ] write seven templates
- [ ] configure paths

Each todo list has

- [ ] a name like work or home
- [ ] todo items have a task description
- [ ] optional due date
- [ ] indicator if its complete

Utilities to check code cleanliness:

1.  flake8 accounts projects tasks
    black --check accounts projects tasks
2.  djhtml -i «path to HTML template»
