from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "items/list.html"
    # context_object_name = "items"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "items/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "items/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "items/update.html"
    fields = [
        "name",
    ]

    def get_success_url(self):
        return reverse_lazy("todo_list_list")


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "items/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/createitem.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/updateitem.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
